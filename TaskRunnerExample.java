import java.util.Comparator;
import java.util.Random;

/**
 * ===============================================================================
 * This is a basic linear implementation of TASK QUEUE elaboration.
 * A more complex and scalable implementation should use ThreadPoolExecutor...
 * ===============================================================================
 */

public class TaskRunnerExample {

	//task queue that will contain priority and not priority request 
	private static int taskQueueCapacity = 100;
	static BaseQueue<BaseRequest> taskQueue = null;
	
	private Priority nextPriority=Priority.High;
	
	//fifo priority queue used to avoid low priority task starvation. Task run proportion 4:1
	//This means that i need to process at least 1 low priority process every 4 high priority processes
	private static org.apache.commons.collections4.queue.CircularFifoQueue<Priority> lastPrioritiesQueue = null;
	private static int maxHighPriority = 4;

	private Random rnd = new Random();
	private Integer taskNumber = 0;
	
	//sorts by priority avoiding low priority task starvation 
	class MyAdvancedPriorityComparator implements Comparator<BaseRequest> {
		public int compare(BaseRequest r1, BaseRequest r2) {						
			int order = 0;
			
			if ((r1==null) && (r2==null))
				return 0;
			if ((r1==null) && (r2!=null))
				return 1;
			else if ((r1!=null) && (r2==null))
				return -1;
			
			
			//sorted by priority low to high
			order = r1.getPriority().compareTo(r2.getPriority());
			
			if (nextPriority.equals(Priority.High)) 
				order = order * -1;
							
			//sorted by number to preserve fifo (compare r1 to r2 for asc-order)
			if (order == 0)
				order = r1.getNumber().compareTo(r2.getNumber());

			return order;
		}
	}
	
	private void insertNewTaskInQueue(Priority priority) throws Exception {
		taskNumber++;
		BaseRequest req = new BaseRequest(taskNumber, priority,
				  "TASK("+taskNumber.toString()+") with priority: "+priority);
		taskQueue.push(req);
	}
	
	public void initQueue() {
		//initialize taskQueue with MyPriorityComparator
		taskQueue = new BaseQueue<BaseRequest>(taskQueueCapacity, new MyAdvancedPriorityComparator());
		
		//init lastPrioritiesQueue with "LOW" value
		lastPrioritiesQueue = new  org.apache.commons.collections4.queue.CircularFifoQueue<Priority>(maxHighPriority);
		while (!lastPrioritiesQueue.isAtFullCapacity())
			lastPrioritiesQueue.add(Priority.Low);
	     
		//System.out.println("-------------------------------------------------");
		//System.out.println("--- Task Queue contents -------------------------");
		//System.out.println("-------------------------------------------------");
		
		
		//fill task queue with random numbers
		int[] randomsArray = rnd.ints(taskQueueCapacity, 0, 2).toArray();
		try {
			for (int val : randomsArray) {
				insertNewTaskInQueue(val==0 ? Priority.Low : Priority.High);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(req.getText());
	}
		
	public void runQueue() {
		System.out.println("-------------------------------------------------");
		System.out.println("--- Task processing Started ---------------------");
		System.out.println("-------------------------------------------------");
		
		Integer executedTasks = 0;
		IRequest req = null;	
		while (true) {
			req = null;			
			
			//reads from queue with MyPriorityComparator sorting logic
			//give low tasks anti starvation priority
			int pSum = 0;
			Priority lastPriority = null;
			for (Priority p : lastPrioritiesQueue) {
				pSum += p.ordinal();
				lastPriority = p;
			}
			nextPriority = (pSum == maxHighPriority ? Priority.Low : Priority.High);	
			if (lastPriority!=nextPriority)
				taskQueue.setSortingNeeded();
					
			
			req = taskQueue.Pop();
		    if (req != null) {
		    	lastPrioritiesQueue.add(req.getPriority());
		    	req.Process();
		    	executedTasks++;
		    } else {
		    	//no more tasks on the queue --> exit 
		    	break;
		    }		    
		    
		    if (executedTasks==70) {
		    	System.out.println("--- 30 new Tasks added to processing ------------");
				
		    	//insert new 30 random tasks
		    	int[] randomsArray = rnd.ints(30, 0, 2).toArray();
		    	try {
					for (int val : randomsArray) {
						insertNewTaskInQueue(val==0 ? Priority.Low : Priority.High);
					}	
				} catch (Exception e) {
					e.printStackTrace();
				}  	
		    }
		    
		}
		System.out.println("-------------------------------------------------");
		System.out.println("--- Task processing Stopped ---------------------");
		System.out.println("-------------------------------------------------");
	}
}
