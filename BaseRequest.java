public class BaseRequest implements IRequest {
	private String text = null;
	private Priority priority = null;
	private Integer number = null;
	
	public BaseRequest(Integer number, Priority priority, String text) {
		this.number = number;
		this.text = text;
		this.priority = priority==null ? Priority.Low : priority;
	}
	
	@Override
	public void Process() {
		System.out.println(this.text);
		
	}

	@Override
	public Priority getPriority() {
		return priority;
	}
	
	public String getText() {
		return text;
	} 
	
	public Integer getNumber() {
		return number;
	}
	
}
