
// Represents a FIFO queue
public interface IQueue<T> {
	
	//Pushes the given request into the queue
	void push(T request) throws Exception;

	/// Pops the first request from the queue
	T Pop();

	/// Sorts the requests contained in the queue by priority ascending.
	/// That is, low priority requests first, then high priority ones.
	void SortByPriority();

}