import java.util.Arrays;
import java.util.Comparator;

public class BaseQueue<T> implements IQueue<T>{

    private T[] queue;
    private int numElements;
    private Comparator<? super T> comparator;
    private boolean sortNeeded;

    @SuppressWarnings("unchecked")
	public BaseQueue(int capacity, Comparator<? super T> comparator) {
		if (capacity < 1)
			throw new IllegalArgumentException();
		this.comparator = comparator;
		this.queue = (T[]) new Object[capacity];
		this.numElements = 0;
		this.sortNeeded = false;
    }
    
	@Override
	public void push(T request) throws Exception {
		//add element to queue
		if (this.numElements==this.queue.length)
			throw new Exception("Actually no space on queue..."); //TODO dinamic growing queue
		this.queue[this.numElements] = request;
		this.numElements++;
		this.sortNeeded = true;
	}
	
	@Override
	public T Pop() {
		// Pops the first request from the queue
		if (numElements==0) 
			return null;
		
		SortByPriority();
		
		T req = (T) queue[0];
		
		for (int i = 0; i < numElements; i++) {
			if (i+1<numElements)
				this.queue[i] = this.queue[i+1];
			else
				this.queue[numElements-1]=null;
		}
		numElements--;
		
		return req;
	}

	public void setSortingNeeded() {
		this.sortNeeded = true;
	}
	
	@Override
	public void SortByPriority() {
		//sorts with the given comparator...
		if (sortNeeded) {
			Arrays.sort(queue, comparator);		
			this.sortNeeded = false;
		}
	}

}
