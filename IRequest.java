

public interface IRequest
{
	Priority getPriority();
	void Process();
}